from functools import partial
import numpy as np
from fhn import fitzhugh_nagumo_discrete_wiki

import matplotlib.pyplot as plt

scenario = {"a": 0.7, "b": 0.8, "tau": 12.5, "I": 0.5}

# limits
a = 0
b = 200
num = 2000
h = (b - a) / num
time_span = np.linspace(a, b, num=num)

traj = []

fhn_disc = partial(fitzhugh_nagumo_discrete_wiki, **scenario)
cur = fhn_disc(x=[0, 0], h=h)
for j in range(num):
    traj.append(cur)
    cur = fhn_disc(x=cur, h=h)

traj = np.array(traj)

fig, ax = plt.subplots(1, 1, figsize=(5, 5))
ax.set(xlabel='t', ylabel='v, w', title='{}'.format(scenario))
v = ax.plot(time_span, traj[:, 0], color='maroon')
w = ax.plot(time_span, traj[:, 1], color='blue', alpha=.5)
ax.legend([v[0], w[0]], ['v', 'w'])
plt.tight_layout()
plt.savefig('img/vw_wiki', dpi=300)
