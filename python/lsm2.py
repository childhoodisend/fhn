import fhn
import plot
import numpy as np

iter = 100
h = 0.25
delta_phi = 0.09
phi_req = False
pfix = ''
if (phi_req):
    pfix = '_phi'

a = 0.2
b = 1.1
tau = 5
I = 0.1

"""
Arguments:
    x : np.array -- col(x_1k, ..., x_nk)
    xT : np.array  -- row(x_1k, ..., x_nk)
    G : np.array  -- matrix NxN
Return:
    np.array : Gamma_k
"""
def Gamma_k(x: np.array, xT: np.array, G: np.array) -> np.array:
    return G - np.matmul(G, x) * np.matmul(xT, G) / (1 + np.matmul(np.matmul(xT, G), x))


def lsm_v():

    G = np.eye(3) * 10000

    phi = 0
    if (phi_req):
        phi = np.random.uniform(low=-delta_phi, high=delta_phi, size=2)
    print(phi)

    prev = fhn.fitzhugh_nagumo_discrete([0, 0], h=h, a=a, b=b, tau=tau, I=I) + phi
    teta = np.array([[np.random.rand()], [h + 1], [-h]])
    tetaT = teta.reshape(-1)
    tetas = [teta]
    for i in range(1, iter):
        x = np.array([[1], [prev[0]], [prev[1] + (prev[0] ** 3)/3]])  # v[i], w[i]
        xT = x.reshape(-1)
        if (phi_req):
            phi = np.random.uniform(low=-delta_phi, high=delta_phi, size=2)
        cur = fhn.fitzhugh_nagumo_discrete(prev.copy(), h=h, a=a, b=b, tau=tau, I=I) + phi
        y = cur[0]  # v[i + 1]

        # lsm
        G = Gamma_k(x, xT, G)
        teta = (teta + np.matmul(G, x) * (y - np.matmul(tetaT, x)))

        tetas.append(teta)
        tetaT = teta.reshape(-1)
        prev = cur.copy()

    tetas = np.array(tetas)
    print(tetas[tetas.shape[0] - 10:, 0, 0] / h)

    y = np.array(tetas[:, 0, 0] / h)
    plot.plot(x=np.linspace(0, iter, iter), y=y, real=I, limit_l=1, limit_r=iter, name='img/lsm/lsm2_I' + pfix, param='$I$')

def lsm_w():

    G = np.eye(3) * 10000

    phi = 0
    if (phi_req):
        phi = np.random.uniform(low=-delta_phi, high=delta_phi, size=2)
    print(phi)

    prev = fhn.fitzhugh_nagumo_discrete([0, 0], h=h, a=a, b=b, tau=tau, I=I) + phi
    teta = np.array([[np.random.rand()], [np.random.rand()], [np.random.rand()]])
    tetaT = teta.reshape(-1)
    tetas = [teta]
    for i in range(1, iter):
        x = np.array([[prev[0]], [1.0], [prev[1]]])  # v[i], w[i]
        xT = x.reshape(-1)
        if (phi_req):
            phi = np.random.uniform(low=-delta_phi, high=delta_phi, size=2)
        cur = fhn.fitzhugh_nagumo_discrete(prev.copy(), h=h, a=a, b=b, tau=tau, I=I) + phi
        y = cur[1]  # w[i + 1]

        # lsm
        G = Gamma_k(x, xT, G)
        teta = (teta + np.matmul(G, x) * (y - np.matmul(tetaT, x)))

        tetas.append(teta)
        tetaT = teta.reshape(-1)
        prev = cur.copy()

    estimated_params = []
    for teta in tetas:
        t = h / teta[0][0]
        aa = t * teta[1][0] / h
        bb = (1 - teta[2][0]) * t / h

        estimated_params.append([t, aa, bb])

    estimated_params = np.array(estimated_params)
    print(estimated_params[estimated_params.shape[0] - 10:])

    y = []
    for param in estimated_params:
        y.append(param[0])
    plot.plot(x=np.linspace(0, iter, iter), y=y, real=tau, limit_l=0, limit_r=iter, name='img/lsm/lsm2_tau'+pfix, param='$\\tau$')

    y = []
    for param in estimated_params:
        y.append(param[1])
    plot.plot(x=np.linspace(0, iter, iter), y=y, real=a, limit_l=0, limit_r=iter, name='img/lsm/lsm2_a'+pfix, param='$a$')

    y = []
    for param in estimated_params:
        y.append(param[2])
    plot.plot(x=np.linspace(0, iter, iter), y=y, real=b, limit_l=0, limit_r=iter, name='img/lsm/lsm2_b'+pfix, param='$b$')


if __name__ == '__main__':
    lsm_v()
    lsm_w()
