import numpy as np
import fhn
import plot

iter = 8000
h = 0.25
delta_phi = 0.09
phi_req = True
pfix = ''
if (phi_req):
    pfix = '_phi'

eps = 1e-4

a = 0.2
b = 1.1
tau = 5
I = 0.1



def gradient_v():
    phi = 0
    if (phi_req):
        phi = np.random.uniform(low=-delta_phi, high=delta_phi, size=2)
    print(phi)
    prev = fhn.fitzhugh_nagumo_discrete([0, 0], h=h, a=a, b=b, tau=tau, I=I) + phi

    teta = np.array([[np.random.rand()], [h + 1], [-h]])
    tetaT = teta.reshape(-1)
    tetas = [teta]
    for i in range(1, iter):
        x = np.array([[1], [prev[0]], [prev[1] + (prev[0] ** 3) / 3]])  # v[i], w[i]
        if (phi_req):
            phi = np.random.uniform(low=-delta_phi, high=delta_phi, size=2)
        cur = fhn.fitzhugh_nagumo_discrete(prev.copy(), h=h, a=a, b=b, tau=tau, I=I) + phi
        y = cur[0]  # v[i + 1]

        # gradient
        gamma_k = 1.0 / i
        teta = teta + gamma_k * (y - np.matmul(tetaT, x)) * x

        tetas.append(teta)
        tetaT = teta.reshape(-1)
        prev = cur.copy()

    tetas = np.array(tetas)
    print(tetas[tetas.shape[0] - 10:, 0, 0] / h)
    y = np.array(tetas[:, 0, 0] / h)
    plot.plot(x=np.linspace(0, iter, iter), y=y, real=I, limit_l=1, limit_r=iter, name='img/gradient/gradient2_I' + pfix, param='$I$')


def gradient_w():
    phi = 0
    if (phi_req):
        phi = np.random.uniform(low=-delta_phi, high=delta_phi, size=2)
    print(phi)
    prev = fhn.fitzhugh_nagumo_discrete([0, 0], h=h, a=a, b=b, tau=tau, I=I) + phi

    teta = np.array([[(h / tau) - eps], [(h * a / tau) + eps], [(1 - b * h / tau) - eps]])
    tetaT = teta.reshape(-1)
    tetas = [teta]
    for i in range(1, iter):
        x = np.array([[prev[0]], [1.0], [prev[1]]])  # v[i], w[i]
        if (phi_req):
            phi = np.random.uniform(low=-delta_phi, high=delta_phi, size=2)
        cur = fhn.fitzhugh_nagumo_discrete(prev.copy(), h=h, a=a, b=b, tau=tau, I=I) + phi
        y = cur[1]  # w[i + 1]

        # gradient
        gamma_k = 1 / i
        teta = teta + gamma_k * (y - np.matmul(tetaT, x)) * x

        tetas.append(teta)
        tetaT = teta.reshape(-1)
        prev = cur.copy()

    estimated_params = []
    for teta in tetas:
        t = h / teta[0][0]
        aa = t * teta[1][0] / h
        bb = (1 - teta[2][0]) * t / h

        estimated_params.append([t, aa, bb])

    estimated_params = np.array(estimated_params)
    print(estimated_params[estimated_params.shape[0] - 10:])

    y = []
    for param in estimated_params:
        y.append(param[0])
    plot.plot(x=np.linspace(0, iter, iter), y=y, real=tau, limit_l=0, limit_r=iter, name='img/gradient/gradient2_tau' + pfix, param='$\\tau$')

    y = []
    for param in estimated_params:
        y.append(param[1])
    plot.plot(x=np.linspace(0, iter, iter), y=y, real=a, limit_l=0, limit_r=iter, name='img/gradient/gradient2_a' + pfix, param='$a$')

    y = []
    for param in estimated_params:
        y.append(param[2])
    plot.plot(x=np.linspace(0, iter, iter), y=y, real=b, limit_l=0, limit_r=iter, name='img/gradient/gradient2_b' + pfix, param='$b$')


if __name__ == '__main__':
    gradient_v()
    gradient_w()
