import numpy as np

"""
fitzhugh_nagumo model
x[0] <-> v, x[1] <-> w
Arguments:
    x : np.array -- array of x
    t : np.array -- TIME
    a : float -- param
    b : float -- param
    tau : float -- param
    I : float -- param
"""


def fitzhugh_nagumo_discrete(x: np.array, h: float, a: float, b: float, tau: float, I: float):
    return np.array([x[0] + h * (x[0] - (x[0] ** 3) / 3 - x[1] + I), x[1] + h * (x[0] + a - b * x[1]) / tau])
