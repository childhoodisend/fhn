import fhn
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


def plot(x, y, real, limit):
    plt.axhline(real, color='maroon', ls='--')
    plt.axhline(y[-1], color='green', ls='--')
    sns.lineplot(y=y[limit:], x=x[limit:])
    plt.show()


if __name__ == '__main__':

    iter = 10000
    h = 0.05

    a = 0.2
    b = 1.1
    tau = 5
    I = 0.1

    # prev = fhn.fitzhugh_nagumo_discrete([0, 0], h=h, a=a, b=b, tau=tau, I=I)
    #
    # teta = np.array([[-h], [h + 1], [-h]])
    # tetaT = teta.reshape(-1)
    # tetas = [teta]
    # for i in range(iter):
    #     x = np.array([[1], [prev[0]], [prev[1] + prev[0] ** 3]])  # v[i], w[i]
    #     cur = fhn.fitzhugh_nagumo_discrete(prev.copy(), h=h, a=a, b=b, tau=tau, I=I)
    #     y = cur[0]  # v[i + 1]
    #
    #     gamma = 1 / (i + 1)
    #     teta = teta + gamma * (y - np.matmul(tetaT, x)) * x
    #     tetas.append(teta)
    #     tetaT = teta.reshape(-1)
    #
    #     prev = cur.copy()
    #
    # tetas = np.array(tetas)
    # I_est = tetas[-1, 0, 0] / h
    # print("I_est = {}".format(I_est))
    #
    # x = np.linspace(0, iter + 1, iter + 1)
    # y = np.array(tetas[:, 0, 0] / h)
    # plt.axhline(I, color='maroon', ls='--')
    # plt.axhline(I_est, color='green', ls='--')
    # sns.lineplot(y=y[400:], x=x[400:])
    # plt.legend(['$I$', '$I_{est}$'])
    # plt.show()

    ################

    prev = fhn.fitzhugh_nagumo_discrete([0, 0], h=h, a=a, b=b, tau=tau, I=I)

    teta = np.array([[-2], [2], [-2]])
    tetaT = teta.reshape(-1)
    tetas = [teta]
    for i in range(1, iter):
        x = np.array([[prev[0]], [1], [prev[1]]])  # v[i], w[i]
        cur = fhn.fitzhugh_nagumo_discrete(prev.copy(), h=h, a=a, b=b, tau=tau, I=I)
        y = cur[1]  # w[i + 1]

        gamma = 1 / (i + 1)
        teta = teta + gamma * (y - np.matmul(tetaT, x)) * x
        tetas.append(teta)
        tetaT = teta.reshape(-1)

        prev = cur.copy()

    estimated_params = []
    for teta in tetas:
        t = h / teta[0][0]
        aa = tau * teta[1][0] / h
        bb = (1 - teta[2][0]) * t / h
        estimated_params.append([t, aa, bb])

    estimated_params = np.array(estimated_params)
    x = np.linspace(0, iter, iter)
    y = []
    for param in estimated_params:
        y.append(param[2])

    plot(x=x, y=y, real=b, limit=5000)
