from functools import partial

import numpy as np
from numpy import random

import pandas as pd
# import scipy.integrate

from fhn import fitzhugh_nagumo_discrete
from plot import plot_save

scenarios = [{"a": 0.2, "b": 1.1, "tau": 5, "I": 0.1},
             {"a": 0.2, "b": 1.1, "tau": 5, "I": 0.2},
             {"a": 0.2, "b": 1.1, "tau": 5, "I": 0.3}]

# limits
a = 0
b = 200
num = 4000
h = (b - a) / num
time_span = np.linspace(a, b, num=num)

traj = [[], [], []]
for i, param in enumerate(scenarios):
    fhn_disc = partial(fitzhugh_nagumo_discrete, **param)
    cur = fhn_disc(x=[0, 0], h=h)
    for j in range(num):
        traj[i].append(cur)
        cur = fhn_disc(x=cur, h=h)

traj = np.array(traj)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

traj_phi = np.array(traj, copy=True)
delta = 0.05
for i, param in enumerate(scenarios):
    for j in range(num):
        phi = random.uniform(-delta, delta)
        traj_phi[i][j][0] += phi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# save plots
plot_save('t', ['v', 'w'], scenarios, time_span, traj, 'img/vw.png')
plot_save('t', ['v + $\Phi$', 'w'], scenarios, time_span, traj_phi, 'img/vw_phi.png')

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# save data to csv
for i, param in enumerate(scenarios):
    dict = {'trajv': traj[i][:, 0], 'trajw': traj[i][:, 1], 'trajv+phi': traj_phi[i][:, 0],
            'trajw+phi': traj_phi[i][:, 1]}
    df = pd.DataFrame(dict)
    df.to_csv('data/{}_data.csv'.format(param), index=False)
