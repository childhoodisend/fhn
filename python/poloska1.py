import numpy as np
import pandas as pd
import plot

delta = 1e-6
delta_phi = delta / 2

""" Poloska algorithm for v
Arguments:
    teta : np.array -- param to estimate
    v : np.array -- array of v_i
    w : np.array -- array of w_i
    delta : float -- range of Poloska
Return: 
    np.array of tetas on each iteration
"""


def poloska_v(teta: np.array, v: np.array, w: np.array, delta_phi: float, delta: float) -> np.array:
    y = v[1]
    x = np.array([[1], [v[0]], [w[0] + (v[0] ** 3) / 3]])
    tetaT = teta.reshape(-1)

    tetas = [teta]
    # gamma = np.random.uniform(0, 2 * (1 - delta_phi / delta))
    gamma = 0.02
    for i in range(v.shape[0] - 1):
        gamma_k = 0
        if np.abs(y - np.matmul(tetaT, x)) > delta:
            gamma_k = gamma / np.linalg.norm(x)

        teta = teta + gamma_k * (y - np.matmul(tetaT, x)) * x
        tetaT = teta.reshape(-1)
        tetas.append(teta)

        x = np.array([[1], [v[i]], [w[i] + (v[i] ** 3) / 3]])
        y = v[i + 1]

    return np.array(tetas), gamma


""" Poloska algorithm for w
Arguments:
    teta : np.array -- param to estimate
    v : np.array -- array of v_i
    w : np.array -- array of w_i
    delta : float -- range of Poloska
Return: 
    np.array of tetas on each iteration
"""


def poloska_w(teta: np.array, v: np.array, w: np.array, delta_phi: float, delta: float):
    y = w[1]
    x = np.array([[v[0]], [1], [w[0]]])
    tetaT = teta.reshape(-1)

    tetas = [teta]
    # gamma = np.random.uniform(0.5, 2 * (1 - delta_phi / delta))
    gamma = 0.02
    for i in range(v.shape[0] - 1):
        gamma_k = 0
        if np.abs(y - np.matmul(tetaT, x)) > delta:
            gamma_k = gamma / np.linalg.norm(x)

        teta = teta + gamma_k * (y - np.matmul(tetaT, x)) * x
        tetaT = teta.reshape(-1)
        tetas.append(teta)

        x = np.array([[v[i]], [1], [w[i]]])
        y = w[i + 1]

    return tetas


if __name__ == '__main__':
    df = pd.read_csv("data/{'a': 0.2, 'b': 1.1, 'tau': 5, 'I': 0.1}_data.csv")
    v = df['trajv+phi'].to_numpy()
    w = df['trajw+phi'].to_numpy()
    # directory o save img traj / traj+phi
    dir = 'traj+phi'

    begin = 0
    end = 50
    num = v.shape[0]
    h = (end - begin) / num
    print("h = {}".format(h))

    # ~~~~~POLOSKA~~~~~
    # -----V-----
    teta = np.array([[np.random.rand()], [h + 1], [-h]])
    tetas_v, gamma = poloska_v(teta=teta, v=v, w=w, delta_phi=delta_phi, delta=delta)

    I = tetas_v[-1][0][0] / h
    print('I = {}'.format(I))
    plot.plot_estimate_error(ylabel='I', real=0.1, estimated_params=np.array(tetas_v[:, 0, 0] / h),
                             time_span=np.linspace(begin, end, num=num), name='img/poloska/{}/I_err.png'.format(dir))
    # -----V-----

    # -----W-----
    # start numbers for tau a b
    teta = np.array([[np.random.rand()], [np.random.rand()], [np.random.rand()]])
    tetas_w = poloska_w(teta=teta, v=v, w=w, delta_phi=delta_phi, delta=delta)
    estimated_params = []
    for teta in tetas_w:
        t = h / teta[0][0]
        aa = t * teta[1][0] / h
        bb = (1 - teta[2][0]) * t / h
        estimated_params.append([t, aa, bb])

    estimated_params = np.array(estimated_params)
    print("tau = {}, a = {}, b = {}".format(estimated_params[-1][0], estimated_params[-1][1], estimated_params[-1][2]))


    plot.plot_estimate_error(ylabel='tau', real=5, estimated_params=estimated_params[:, 0],
                             time_span=np.linspace(begin, end, num=num), name='img/poloska/{}/tau_err.png'.format(dir))

    plot.plot_estimate_error(ylabel='a', real=0.2, estimated_params=estimated_params[:, 1],
                             time_span=np.linspace(begin, end, num=num), name='img/poloska/{}/a_err.png'.format(dir))

    plot.plot_estimate_error(ylabel='b', real=1.1, estimated_params=estimated_params[:, 2],
                             time_span=np.linspace(begin, end, num=num), name='img/poloska/{}/b_err.png'.format(dir))
    # ~~~~~POLOSKA~~~~~
