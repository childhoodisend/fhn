import numpy as np
import pandas as pd

import plot

"""
Arguments:
    x : np.array -- col(x_1k, ..., x_nk)
    xT : np.array  -- row(x_1k, ..., x_nk)
    G : np.array  -- matrix NxN
Return:
    np.array : Gamma_k
"""


def Gamma_k(x: np.array, xT: np.array, G: np.array) -> np.array:
    return G - np.matmul(G, x) * np.matmul(xT, G) / (1 + np.matmul(np.matmul(xT, G), x))


""" 
    Least Square Method for v
Arguments:
    teta : np.array -- param to estimate
    G : np.array -- gamma matrix
    v : np.array -- array of v_i
    w : np.array -- array of w_i
    x0 : np.array -- array ([[1], [v[0]], [w[0] + v[0] ** 3]])
    y0 : float -- v[1]
Return:
    np.array of tetas on each iteration
"""


def LSM_v(teta: np.array, G: np.array, v: np.array, w: np.array) -> np.array:
    y = v[1]
    x = np.array([[1], [v[0]], [w[0] + (v[0] ** 3) / 3]])
    xT = x.reshape(-1)
    tetaT = teta.reshape(-1)
    tetas = [teta]
    for i in range(v.shape[0] - 1):
        G = Gamma_k(x, xT, G)

        teta = (teta + np.matmul(G, x) * (y - np.matmul(tetaT, x)))
        tetas.append(teta)
        tetaT = teta.reshape(-1)

        x = np.array([[1], [v[i]], [w[i] + (v[i] ** 3) / 3]])
        xT = x.reshape(-1)

        y = v[i + 1]

    return np.array(tetas)


""" 
    Least Square Method for w
Arguments:
    teta : np.array -- param to estimate
    G : np.array -- gamma matrix
    v :np.array -- array of v_i
    w : np.array --array of w_i
    x0 : np.array array of ([[v[0]], [1], [w[0]]])
    y0 : float  w[1]
Return:
    np.array of tetas on each iteration
"""


def LSM_w(teta: np.array, G: np.array, v: np.array, w: np.array) -> np.array:
    y = w[1]
    x = np.array([[v[0]], [1], [w[0]]])
    xT = x.reshape(-1)
    tetaT = teta.reshape(-1)
    tetas = [teta]
    for i in range(v.shape[0] - 1):
        G = Gamma_k(x, xT, G)
        teta = (teta + np.matmul(G, x) * (y - np.matmul(tetaT, x)))
        tetas.append(teta)

        tetaT = teta.reshape(-1)
        x = np.array([[v[i]], [1], [w[i]]])
        y = w[i + 1]
        xT = x.reshape(-1)

    return np.array(tetas)


if __name__ == '__main__':
    df = pd.read_csv("data/{'a': 0.2, 'b': 1.1, 'tau': 5, 'I': 0.1}_data.csv")
    v = df['trajv+phi'].to_numpy()
    w = df['trajw+phi'].to_numpy()
    # directory o save img traj / traj+phi
    dir = 'traj+phi'

    begin = 0
    end = 50
    num = v.shape[0]
    h = (end - begin) / num
    print("h = {}".format(h))

    # ~~~~~LSM~~~~~
    # -----V-----
    # start number for I
    teta = np.array([[np.random.rand()], [h + 1], [-h]])
    G0 = np.eye(3) * 10000  # единичная матрица * 10000

    tetas_v = LSM_v(teta=teta, G=G0, v=v, w=w)
    I = tetas_v[-1][0][0] / h
    print("I = {}".format(I))

    plot.plot_estimate_error(ylabel='I', real=0.1, estimated_params=np.array(tetas_v[:, 0, 0] / h),
                             time_span=np.linspace(begin, end, num=num), name='img/lsm/{}/I_err.png'.format(dir))
    # -----V-----

    # -----W-----
    # start numbers for tau a b
    teta = np.array([[np.random.rand()], [np.random.rand()], [np.random.rand()]])
    G0 = np.eye(3) * 10000  # единичная матрица * 10000

    tetas_w = LSM_w(teta=teta, G=G0, v=v, w=w)
    estimated_params = []
    for teta in tetas_w:
        t = h / teta[0][0]
        aa = t * teta[1][0] / h
        bb = (1 - teta[2][0]) * t / h
        estimated_params.append([t, aa, bb])

    estimated_params = np.array(estimated_params)
    print("tau = {}, a = {}, b = {}".format(estimated_params[-1][0], estimated_params[-1][1], estimated_params[-1][2]))

    plot.plot_estimate_error(ylabel='tau', real=5, estimated_params=estimated_params[:, 0],
                             time_span=np.linspace(begin, end, num=num), name='img/lsm/{}/tau_err.png'.format(dir))

    plot.plot_estimate_error(ylabel='a', real=0.2, estimated_params=estimated_params[:, 1],
                             time_span=np.linspace(begin, end, num=num), name='img/lsm/{}/a_err.png'.format(dir))

    plot.plot_estimate_error(ylabel='b', real=1.1, estimated_params=estimated_params[:, 2],
                             time_span=np.linspace(begin, end, num=num), name='img/lsm/{}/b_err.png'.format(dir))

    # -----W-----
    # ~~~~~LSM~~~~~
