import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

"""
    plot and save
Arguments:
    xlabel : char  -- 't'
    ylabel : np.array  -- 'v, w'
    scenarios : dict -- dict of current scene
    time_span : np.array -- TIME
    traj : np.array -- trajectory
    name : str -- name of file to save
"""


def plot_save(xlabel, ylabel, scenarios, time_span, traj, name):
    fig, ax = plt.subplots(1, len(scenarios), figsize=(5 * len(scenarios), 5))
    for i, param in enumerate(scenarios):
        ax[i].set(xlabel=xlabel, ylabel=ylabel[0] + ',' + ylabel[1], title='{}'.format(param))
        v = ax[i].plot(time_span, traj[i][:, 0], color='maroon')
        w = ax[i].plot(time_span, traj[i][:, 1], color='blue', alpha=.5)
        ax[i].legend([v[0], w[0]], [ylabel[0], ylabel[1]])
    plt.tight_layout()
    plt.savefig(name, dpi=300)
    plt.show()


"""
    plot estimated param and real param
Arguments:
    ylabel : np.array of param labels
    real : np.array -- array of real params
    estimated_params : np.array -- array of estimated params throw time
    time_span : np.array -- TIME
    name : str -- name of file to save
"""


def plot_estimate_error(ylabel: np.array, real: np.array, estimated_params: np.array, time_span: np.array, name: str):
    line = sns.lineplot(x=time_span[50:], y=estimated_params[50:], color='blue', alpha=.5)
    line.axhline(real, color='maroon', ls='--')
    plt.legend(['estimated ' + ylabel, 'real ' + ylabel])
    plt.savefig(name, dpi=300)
    plt.close()
    plt.show()
